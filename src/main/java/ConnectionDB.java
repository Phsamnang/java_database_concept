import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDB {
  Connection getConnection(){
      Connection connection =null;
      try {
          connection= DriverManager.getConnection("jdbc:postgresql://localhost:5432/person","postgres","1234");
      } catch (SQLException e) {
          e.printStackTrace();
      }
      return connection;
  }
}
