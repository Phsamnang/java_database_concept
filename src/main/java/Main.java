import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);

    static Person getInforPerson() {
        System.out.print("Enter name:");
        String name = scanner.nextLine();
        System.out.print("Enter address:");
        String address = scanner.nextLine();
        return new Person(name, address);
    }

    static void insertToDB(Connection connection) {
        String insert = "INSERT INTO person VALUES(default, ?, ?)";
        Person ps = getInforPerson();
        try {
            PreparedStatement statement = connection.prepareStatement(insert);
            statement.setString(1, ps.getName());
            statement.setString(2, ps.getAddress());
            int row = statement.executeUpdate();
            if (row > 0) {
                System.out.println("Success");
            } else {
                System.out.println("fail");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static void updateDB(Connection connection) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter ID:");
        int id = scan.nextInt();
        String update = "UPDATE person SET name=?,address=? WHERE id=?";
        Person ps = getInforPerson();
        try {
            PreparedStatement statement = connection.prepareStatement(update);
            statement.setString(1, ps.getName());
            statement.setString(2, ps.getAddress());
            statement.setInt(3, id);
            int row = statement.executeUpdate();
            if (row > 0) {
                System.out.println("Success");
            } else {
                System.out.println("fail");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ConnectionDB connectionDB = new ConnectionDB();
        Connection connection = connectionDB.getConnection();
        Scanner sn = new Scanner(System.in);
        int num = 0;
        do {
            System.out.println("Number 1 for Insert and 2 for update");
            System.out.print("Please Enter number:");
            num = sn.nextInt();
            switch (num) {
                case 1:
                    insertToDB(connection);
                    break;
                case 2:
                    updateDB(connection);
                    break;
                default:
                    break;
            }
        } while (num!=3);
        //insertToDB(connection);

    }
}